APP=chestscan
PREFIX?=/usr/local

all:
	gcc -Wall -std=c99 -pedantic -O3 -o ${APP} -Isrc -Isrc/visionutils src/*.c src/visionutils/*.c -lm -fopenmp

debug:
	gcc -Wall -std=c99 -pedantic -g -o ${APP} -Isrc -Isrc/visionutils src/*.c src/visionutils/*.c -lm -fopenmp

install:
	cp ${APP} ${DESTDIR}${PREFIX}/bin

clean:
	rm -f src/*.plist src/visionutils/*.plist ${APP} *.png
