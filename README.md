# Chest X-Ray scanning tool

<img src="https://code.freedombone.net/bashrc/chestscan/raw/master/img/3_ribcage_lines2.jpg?raw=true" width=600/>

This is a utility for extracting the ribcage region from chest x-ray images to create an image suitable for training a classifier and as a possible precursor to computer assisted diagnosis.

It is possible for neural nets or other machine learning methods to classify these types of images merely by directly shovelling them into a training set. However, learning systems can get hung up on the smallest and least significant things. So if in the training data many patients in a particular disease category happen to have their arm at a particular angle due to the shape of bed in a particular hospital then a neural net will learn that and use it, even though it's actually an irrelevant factor for diagnosis.

Extracting the region of interest in a standardised pose gives any subsequent machine learning a better chance of producing useful results.

Image source: https://www.kaggle.com/paultimothymooney/detecting-pneumonia-in-x-ray-images/data

To get the images, download from the above site (you will need a login), then unzip.

To compile and install:

``` bash
make
sudo make install
```

You should also have [imagemagick](https://www.imagemagick.org/script/index.php) installed.

To run:

``` bash
chestscan -f img/normal/example1.jpeg -d detect.png
```

Two images will be produced. *detect.png* shows the results of detecting the ribcage and *result.png* is the extracted ribcage image.

If you need to process a lot of images you can use a script, such as:

``` bash
#!/bin/bash

if [ ! -d training ]; then
    mkdir training
fi

if [ -f training/images.log ]; then
    rm training/images.log
fi

ctr=1
for filename in chest_xray/train/NORMAL/*.jpeg; do
    chestscan -f "$filename" -o "training/normal_${ctr}.png"
    echo "normal_${ctr}.png $filename" >> training/images.log
    ctr=$((ctr+1))
done

ctr=1
for filename in chest_xray/train/PNEUMONIA/*.jpeg; do
    chestscan -f "$filename" -o "training/pneumonia_${ctr}.png"
    echo "pneumonia_${ctr}.png $filename" >> training/images.log
    ctr=$((ctr+1))
done
```

To see the command options:

``` bash
chestscan --help
```

## How it works

Convert image to PNG format.

Create a smaller version of the image for the purpose of detecting the ribcage region.

Find the spinal axis by looking for maximum lateral symmetry. This makes the assumption that the body is roughly symmetrical. This gives a line fitting the orientation of the spine.

<img src="https://code.freedombone.net/bashrc/chestscan/raw/master/img/1_spinal_axis.jpg?raw=true" width=600/>

Look for high intensity symmetry around the spinal axis, with a dead region to ensure that the spine isn't detected.

Apply some heuristics such as orientation angle to rule out things which are not the outer perimeter of the ribcage.

<img src="https://code.freedombone.net/bashrc/chestscan/raw/master/img/2_ribcage_segments.jpg?raw=true" width=600/>

Convert the detected ribcage perimeter points into three line segments for each side. This simplifies subsequent processing.

<img src="https://code.freedombone.net/bashrc/chestscan/raw/master/img/3_ribcage_lines.jpg?raw=true" width=600/>

Extract the ribcage region to a separate image using the full resolution original image so that detail is preserved. It is mapped into a standardised coordinate frame so that pose variations are removed.

Calculate the mean light and mean dark pixel values for the ribcage image and enhance contrast. This corrects for different exposures. Also apply a minimum threshold so that any low intensity lung structures stand out better from the background.

<img src="https://code.freedombone.net/bashrc/chestscan/raw/master/img/4_ribcage_image.jpg?raw=true" width=600/>
