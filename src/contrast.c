/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Enhance contrast using mean light and mean dark
 *  Copyright (c) 2018, Bob Mottram
 *  bob@freedombone.net
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "skeleton.h"

/**
 * @brief Enhance the contrast of an image
 * @param img Image to be updated
 * @param width Width of the image
 * @param height Height of the image
 * @param bitsperpixel Number of bits per pixel
 * @param threshold Minimum threshold
 * @return zero on success
 */
int dark_light_contrast(unsigned char img[],
                        unsigned int width, unsigned int height,
                        int bitsperpixel,
                        unsigned char threshold)
{
    unsigned char dark, light;
    int y, x, n, b;
    const int samplingStepSize = 2;
    const int sampling_radius_percent = 5;
    int bytes_per_pixel = bitsperpixel/8;

    /* detect mean dark and mean light using reflectance histogram */
    if (bytes_per_pixel == 3)
        darklight_colour(img, width, height,
                         samplingStepSize,
                         sampling_radius_percent,
                         &dark, &light);
    else
        darklight(img, width, height,
                  samplingStepSize,
                  sampling_radius_percent,
                  &dark, &light);

    /* adjust the dark threshold down for a bit of tollerance */
    dark = (unsigned char)((int)dark / 2);

    /* adjust the light threshold up for a bit of tollerance */
    light=light+((255-light)/2);

    /* remap the image into the new range */
    for (y = 0; y < height; y++) {
        for (x = 0; x < width; x++) {
            n = (y*width + x)*bytes_per_pixel;
            for (b = 0; b < bytes_per_pixel; b++) {
                if (img[n+b] < dark) {
                    img[n+b] = dark;
                    continue;
                }
                if (img[n+b] > light) {
                    img[n+b] = light;
                    continue;
                }
                img[n+b] = (img[n+b] - dark)*255/(light-dark);
                if (img[n+b] < threshold)
                    img[n+b] = 0;
            }
        }
    }

    return 0;
}
