/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Visionutils - example computer vision functions
 *  Copyright (c) 2011-2015, Bob Mottram
 *  bob@freedombone.net
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/


#include "visionutils.h"

/* convert an image from mono to colour */
void mono_to_colour(unsigned char * img, int width, int height,
                    int bitsperpixel,
                    unsigned char * colour)
{
    int i,j;
    int bytesperpixel = bitsperpixel/8;

    for (i = 0; i < width*height; i++)
        for (j = 0; j < bytesperpixel; j++)
            colour[i*bytesperpixel + j] = img[i];
}

/* convert an image from colour to mono */
void colour_to_mono(unsigned char * img, int width, int height,
                    int bitsperpixel,
                    unsigned char * mono)
{
    int i,j,v;
    int bytesperpixel = bitsperpixel/8;

    for (i = 0; i < width*height; i++) {
        v = 0;
        for (j = 0; j < bytesperpixel; j++) {
            v += img[i*bytesperpixel + j];
        }
        mono[i+j] = v/bytesperpixel;
    }
}

/* convert a mono image to a bitwise image, compressing it
   by a factor of 8. You may previously have used an adaptive
   threshold to binarise the image */
void mono_to_bitwise(unsigned char * img, int width, int height,
                     unsigned char * bitwise)
{
    int i;
    int bit_index = 0;
    int byte_index = 0;
    int bitbin = 1;

    memset((void*)bitwise, '\0',
           width*height/8 * sizeof(unsigned char));

    for (i = 0; i < width*height; i++) {
        if (img[i] > 127)
            bitwise[byte_index] |= bitbin;

        bit_index++;
        bitbin *= 2;
        if (bit_index >= 8) {
            bit_index = 0;
            bitbin = 1;
            byte_index++;
        }
    }
}

void bitwise_to_mono(unsigned char * bitwise, int width, int height,
                     unsigned char * img)
{
    int i;
    int bit_index = 0;
    int byte_index = 0;
    int bitbin = 1;

    memset((void*)img, '\0',
           width*height * sizeof(unsigned char));

    for (i = 0; i < width*height; i++) {
        if (bitwise[byte_index] & bitbin)
            img[i] = 255;

        bit_index++;
        bitbin *= 2;
        if (bit_index >= 8) {
            bit_index = 0;
            bitbin = 1;
            byte_index++;
        }
    }
}

void bitwise_to_colour(unsigned char * bitwise, int width, int height,
                       unsigned char * img, int bytesperpixel)
{
    int i, j;
    int bit_index = 0;
    int byte_index = 0;
    int bitbin = 1;

    memset((void*)img, '\0',
           width*height*bytesperpixel * sizeof(unsigned char));

    for (i = 0; i < width*height; i++) {
        if (bitwise[byte_index] & bitbin) {
            for (j = 0; j < bytesperpixel; j++) {
                img[i*bytesperpixel + j] = 255;
            }
        }

        bit_index++;
        bitbin *= 2;
        if (bit_index >= 8) {
            bit_index = 0;
            bitbin = 1;
            byte_index++;
        }
    }
}

int save_bitwise(char * filename,
                 unsigned char * bitwise, int width, int height)
{
    FILE * fp = fopen(filename, "wb");
    if (!fp) return -1;
    fwrite(bitwise, width*height/8, 1, fp);
    fclose(fp);
    return 0;
}

/* it is assumed that you know the dimensions of the image in advance */
int load_bitwise(char * filename,
                 unsigned char * bitwise, int width, int height)
{
    FILE * fp = fopen(filename, "rb");
    if (!fp) return -1;
    if (fread(bitwise, width*height/8, 1, fp) == 0) {
        fclose(fp);
        return -1;
    }
    fclose(fp);
    return 0;
}
