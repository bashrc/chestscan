/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Visionutils - Centre of gravity
 *  Copyright (c) 2018, Bob Mottram
 *  bob@freedombone.net
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "visionutils.h"

/* Returns the centre of gravity of the given image */
void centre_of_gravity(unsigned char * img,
                       int img_width, int img_height,
                       int bitsperpixel,
                       int * cx, int * cy)
{
    int bytes_per_pixel = bitsperpixel/8;
    int n = (img_width*img_height*bytes_per_pixel)-1;
    int total = 0;

    *cx = 0;
    *cy = 0;

    if (bytes_per_pixel > 1) {
        for (int y = img_height-1; y >= 0 ; y--) {
            for (int x = img_width-1; x >= 0; x--) {
                for (int c = bytes_per_pixel-1; c >= 0; c--, n--) {
                    *cx = *cx + ((int)img[n]*x);
                    *cy = *cy + ((int)img[n]*y);
                    total += (int)img[n];
                }
            }
        }
    }
    else {
        for (int y = img_height-1; y >= 0 ; y--) {
            for (int x = img_width-1; x >= 0; x--, n--) {
                *cx = *cx + ((int)img[n]*x);
                *cy = *cy + ((int)img[n]*y);
                total += (int)img[n];
            }
        }
    }

    if (total > 0) {
        *cx = *cx / total;
        *cy = *cy / total;
    }
}
