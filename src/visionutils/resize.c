/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Visionutils - centre/surround functions
 *  Copyright (c) 2013-2015, Bob Mottram
 *  bob@freedombone.net
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "visionutils.h"

/**
 * @brief makes an image larger using an appropriate background colour
 * @param img The original image array
 * @param width Width of the original image
 * @param height Height of the original image
 * @param bitsperpixel Number of bits per pixel in the image
 * @param enlarged The returned enlarged image
 * @param enlarged_width Width of the enlarged image
 * @param enlarged_height Height of the enlarged image
 * @return zero on success
 */
int enlarge_image(unsigned char img[], int width, int height,
                  int bitsperpixel,
                  unsigned char enlarged[],
                  int enlarged_width, int enlarged_height)
{
    int i, c, tx, ty, bx, by, x, y, n0=0, n1;
    int bytes_per_pixel = bitsperpixel/8;
    unsigned char background[4];

    if (enlarged_width < width) {
        return -1;
    }
    if (enlarged_height < height) {
        return -2;
    }
    if (bytes_per_pixel > 4) {
        return -3;
    }

    /* location of the original image within the larger one */
    tx = (enlarged_width - width)/2;
    ty = (enlarged_height - height)/2;
    bx = tx + width;
    by = ty + height;

    /* get the background colour */
    for (c = 0; c < bytes_per_pixel; c++) {
        background[c] = img[c];
    }

    /* set the background colour in the enlarged image */
    c = 0;
    for (i = 0; i < enlarged_width*enlarged_height*bytes_per_pixel; i++) {
        enlarged[i] = background[c++];
        if (c >= bytes_per_pixel) c = 0;
    }

    /* copy the original into the enlarged */
    for (y = ty; y < by; y++) {
        for (x = tx; x < bx; x++, n0+=bytes_per_pixel) {
            n1 = (y*enlarged_width + x)*bytes_per_pixel;
            for (c = 0; c < bytes_per_pixel; c++)
                enlarged[n1+c] = img[n0+c];
        }
    }
    return 0;
}

/**
 * @brief Changes the size of an image
 * @param img The original image array
 * @param width Width of the original image
 * @param height Height of the original image
 * @param bitsperpixel Number of bits per pixel in the image
 * @param resized The returned enlarged image
 * @param resized_width Width of the enlarged image
 * @param resized_height Height of the enlarged image
 * @return zero on success
 */
int resize_image(unsigned char img[], int width, int height,
                 int bitsperpixel,
                 unsigned char resized[],
                 int resized_width, int resized_height)
{
    int c, x, y, x2, y2, n1, n2;
    int bytes_per_pixel = bitsperpixel/8;

    if ((resized_width == width) && (resized_height == height)) {
        memcpy(resized, img, width*height*bytes_per_pixel);
        return 0;
    }

    if ((resized_width > width) || (resized_height > height)) {
        return enlarge_image(img, width, height, bitsperpixel,
                             resized, resized_width, resized_height);
    }

    if (bytes_per_pixel > 4) {
        return -1;
    }

    memset(resized, '\0', resized_width*resized_height*bytes_per_pixel);

    for (y = resized_height-1; y >= 0; y--) {
        y2 = y * height / resized_height;
        for (x = resized_width-1; x >= 0; x--) {
            x2 = x * width / resized_width;
            n1 = (y2*width + x2)*bytes_per_pixel;
            n2 = (y*resized_width + x)*bytes_per_pixel;
            /* NOTE: this isn't optimal, but probably good enough */
            for (c = bytes_per_pixel-1; c >= 0; c--)
                if (resized[n2+c] == 0)
                    resized[n2+c] = img[n1+c];
                else
                    resized[n2+c] = (unsigned int)((int)img[n1+c]+(int)resized[n2+c])/2;
        }
    }
    return 0;
}
