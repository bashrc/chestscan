/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Visionutils - example computer vision functions
 *  Copyright (c) 2011-2015, Bob Mottram
 *  bob@freedombone.net
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/


#include "visionutils.h"

/* apply erosion morphology operator */
void erosion(unsigned char * img,
             int width, int height,
             int itterations)
{
    int x,y,i,n,ctr;
    unsigned char * buffer;

    buffer = (unsigned char*)malloc(width*height);

    for (i = 0; i < itterations; i++) {
        for (y = 1; y < height-1; y++) {
            for (x = 1; x < width-1; x++) {
                n = y*width+x;
                buffer[n] = img[n];
                if (img[n] != 0) {
                    ctr=0;
                    if (img[n-1] != 0) ctr++;
                    if (img[n-1-width] != 0) ctr++;
                    if (img[n-width] != 0) ctr++;
                    if (img[n-width+1] != 0) ctr++;
                    if (img[n+1] != 0) ctr++;
                    if (img[n+width+1] != 0) ctr++;
                    if (img[n+width] != 0) ctr++;
                    if (img[n+width-1] != 0) ctr++;

                    if (ctr < 5) buffer[n]=0;
                }
            }
        }
        for (n = 0; n < width*height; n++) {
            img[n] = buffer[n];
        }
    }
    free(buffer);
}

/* apply dilation morphology operator */
void dilation(unsigned char * img,
              int width, int height,
              int itterations)
{
    int x,y,i,n,ctr;
    unsigned char * buffer;

    buffer = (unsigned char*)malloc(width*height);

    for (i = 0; i < itterations; i++) {
        for (y = 1; y < height-1; y++) {
            for (x = 1; x < width-1; x++) {
                n = y*width+x;
                buffer[n] = img[n];
                if (img[n] == 0) {
                    ctr=0;
                    if (img[n-1]==0) ctr++;
                    if (img[n-1-width]==0) ctr++;
                    if (img[n-width]==0) ctr++;
                    if (img[n-width+1]==0) ctr++;
                    if (img[n+1]==0) ctr++;
                    if (img[n+width+1]==0) ctr++;
                    if (img[n+width]==0) ctr++;
                    if (img[n+width-1]==0) ctr++;

                    if (ctr<5) buffer[n]=255;
                }
            }
        }
        for (n = 0; n < width*height; n++) {
            img[n] = buffer[n];
        }
    }
    free(buffer);
}
