/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Visionutils - Rotation functions
 *  Copyright (c) 2018, Bob Mottram
 *  bob@freedombone.net
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "visionutils.h"

/* create a lookup table which can be used to detect orientation */
void create_rotation_lookup(int img_width, int img_height,
                            int no_of_rotations,
                            short lookup[])
{
    int cx = img_width / 2;
    int cy = img_height / 2;

    lookup = (short*)malloc(img_width*img_height*no_of_rotations*
                          2*sizeof(short));

    for (int r = 0; r < no_of_rotations; r++) {
        float offset = (3.1415927f*2) * r / (float)no_of_rotations;
        int n = img_width*img_height*r*2;
        for (int y = 0; y < img_height; y++) {
            int dy = y - cy;
            for (int x = 0; x < img_width; x++, n+=2) {
                int dx = x - cx;

                /* calculate the rotated angle */
                float hyp = (float)sqrt((dx*dx)+(dy*dy));
                float angle = (float)acos((float)dx/hyp);
                if (dy < 0) angle = (2*3.1415927f) - angle;
                angle += offset;

                /* calculate the rotated position */
                float x2 = cx + (hyp * (float)sin(angle));
                float y2 = cy + (hyp * (float)cos(angle));

                lookup[n] = (short)x2;
                lookup[n + 1] = (short)y2;
            }
        }
    }
}

/* Given an input image, a set of temnplate images (one per rotation)
   and a rotation lookup table return the translation offset and
   index of the closest matching rotation */
int match_image_with_rotation(int img_width, int img_height,
                              unsigned char input[],
                              unsigned char template[], short rotation_lookup[],
                              int no_of_rotations,
                              int *offset_x, int *offset_y, float * score)
{
    float max = 0;
    int best_index = 0;
    int off_x = 0, off_y = 0;

    centre_of_gravity(input,
                      img_width, img_height, 8,
                      &off_x, &off_y);
    off_x -= (img_width/2);
    off_y -= (img_height/2);

    *offset_x = off_x;
    *offset_y = off_y;
    *score = 0;

    for (int r = 0; r < no_of_rotations; r++) {
        int off = img_width*img_height*r;
        short * lookup = &rotation_lookup[off*2];
        float match = 0;
        int hits = 0;

        for (int n = img_width*img_height-1; n >= 0; n--) {
            int x = (int)lookup[n*2] - off_x;
            if ((x >= 0) && (x < img_width)) {
                int y = (int)lookup[n*2 + 1] - off_y;
                if ((y >= 0) && (y < img_height)) {
                    int n2 = y*img_width + x + off;
                    int diff = (int)input[n] - (int)template[n2];
                    match += (float)(diff*diff);
                    hits++;
                }
            }
        }
        if (hits > 0) {
            match = 1.0f / (1.0f + (match/(float)hits));

            if (match >= max) {
                *score = match;
                max = match;
                best_index = r;
            }
        }
    }

    return best_index;
}

/* Moves the template for the given rotation index closer to the input image */
void learn_image_with_rotation(int img_width, int img_height,
                               unsigned char input[],
                               unsigned char template[], short rotation_lookup[],
                               int no_of_rotations,
                               int offset_x, int offset_y, int rotation_index)
{
    for (int r = 0; r < no_of_rotations; r++) {
        int r2 = (r + rotation_index) % no_of_rotations;
        int off = img_width*img_height*r2;
        short * lookup = &rotation_lookup[off*2];

        for (int n = img_width*img_height-1; n >= 0; n--) {
            int x = (int)lookup[n*2] - offset_x;
            if ((x >= 0) && (x < img_width)) {
                int y = (int)lookup[n*2 + 1] - offset_y;
                if ((y >= 0) && (y < img_height)) {
                    int n2 = y*img_width + x + off;
                    int diff = (int)input[n] - (int)template[n2];
                    if (diff > 0) template[n2]++;
                    if (diff < 0) template[n2]--;
                }
            }
        }
    }
}
