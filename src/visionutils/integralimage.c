/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Visionutils - integral images
 *  Copyright (c) 2013-2015, Bob Mottram
 *  bob@freedombone.net
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/


#include "visionutils.h"

/* update the integral image, using the given mono bitmap */
void update_integral_image(unsigned char * img,
                           int img_width, int img_height,
                           int bitsperpixel,
                           long * integral_image)
{
    int x, y, p, n = img_width, c;

    if (bitsperpixel == 8) {
        for (y = 1; y < img_height; y++) {
            p = 0;
            for (x = 0; x < img_width; x++, n++) {
                p += img[n];
                integral_image[n] =
                    p + integral_image[n - img_width];
            }
        }
        return;
    }
    if (bitsperpixel == 24) {
        for (y = 1; y < img_height; y++) {
            p = 0;
            for (x = 0; x < img_width; x++, n++) {
                for (c = 0; c < 3; c++) {
                    p += img[n*3 + c];
                }
                integral_image[n] =
                    p + integral_image[n - img_width];
            }
        }
        return;
    }
    if (bitsperpixel == 32) {
        for (y = 1; y < img_height; y++) {
            p = 0;
            for (x = 0; x < img_width; x++, n++) {
                for (c = 0; c < 3; c++) {
                    p += img[n*4 + c];
                }
                integral_image[n] =
                    p + integral_image[n - img_width];
            }
        }
        return;
    }
}

/* get the total pixel value for the given area */
long get_integral(long * integral_image,
                  int tx, int ty, int bx, int by, int img_width)
{
    int n1 = ty * img_width;
    int n2 = by * img_width;

    return (integral_image[n2 + bx] +
            integral_image[n1 + tx] -
            (integral_image[n2 + tx] +
             integral_image[n1 + bx]));
}
