/*********************************************************************
* Software License Agreement (BSD License)
*
*  Visionutils - example computer vision functions
*  Copyright (c) 2011-2015, Bob Mottram
*  bob@freedombone.net
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/


#ifndef VISIONUTILS_HEADERS_H
#define VISIONUTILS_HEADERS_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <ctype.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include <omp.h>

#define GPR_MISSING_VALUE 9999

struct line_segments {
  int image_border;
  int ignore_periphery;
  int max_members;
  int * members;
  int * no_of_members;
  int max_segments;
  int minimum_segment_length;
  int no_of_segments;
};



/* histogram.c */

int histogram_mean_reflectance(int * histogram);

void region_histogram(
              unsigned char * img, int width, int height,
              int tx, int ty, int bx, int by,
              int bitsperpixel,
              int * histogram);

/* threshold.c */

void adaptive_threshold(
            unsigned char * img, int width, int height,
            int bitsperpixel,
            int radius,
            unsigned char * thresholded);

int meanlight_threshold(unsigned char * img, int width, int height,
                        int bitsperpixel, int threshold,
                        unsigned char * thresholded);

/* convert.c */

void bitwise_to_mono(unsigned char * bitwise, int width, int height,
                     unsigned char * img);

void bitwise_to_colour(unsigned char * bitwise, int width, int height,
                       unsigned char * img, int bytesperpixel);

void mono_to_colour(unsigned char * img, int width, int height,
            int bitsperpixel,
            unsigned char * colour);

void colour_to_mono(unsigned char * img, int width, int height,
            int bitsperpixel,
            unsigned char * mono);

void mono_to_bitwise(unsigned char * img, int width, int height,
                     unsigned char * bitwise);

int save_bitwise(char * filename,
                 unsigned char * bitwise, int width, int height);

int load_bitwise(char * filename,
                 unsigned char * bitwise, int width, int height);

/* erosiondilation.c */

void erosion(
         unsigned char * img,
         int width, int height,
         int itterations);

void dilation(
          unsigned char * img,
          int width, int height,
          int itterations);

/* dark and light thresholds */

void darklight(unsigned char * img,
               int width, int height,
               int samplingStepSize,
               int sampling_radius_percent,
               unsigned char * dark, unsigned char * light);
void darklight_colour(unsigned char * img,
                      int width, int height,
                      int samplingStepSize,
                      int sampling_radius_percent,
                      unsigned char * dark, unsigned char * light);

/* colours.h */

void remove_channel(unsigned char * img, int width, int height, int bitsperpixel,
            int channel);
int rgb_to_hsv(unsigned char img[],
               int width, int height, int bitsperpixel,
               unsigned char img_hsv[]);
int rgb_to_hsl(unsigned char img[],
               int width, int height, int bitsperpixel,
               unsigned char img_hsl[]);
int rgb_to_cielab(unsigned char img[],
                  int width, int height, int bitsperpixel,
                  unsigned char img_cielab[]);
void rgb_colour_reduce(unsigned char img[],
                       int width, int height);

/* edges.c */

void detect_edges(
          unsigned char * img,
          int width, int height,
          float threshold, float radius);


void get_line_segments(unsigned char * edgesImage, int width, int height,
               struct line_segments * segments);

void free_line_segments(struct line_segments * segments);

void show_line_segments(struct line_segments * segments,
            unsigned char * result, int width,
            int result_bitsperpixel);

/* filters.c */

void filter_image(unsigned char * img,
                  int img_width, int img_height,
                  int bitsperpixel,
                  float * filter,
                  int filter_width,
                  unsigned char * output_img,
                  int output_img_width,
                  int output_img_height,
                  int output_bitsperpixel);

/* random.c */

int rand_num(unsigned int * seed);

/* som.c */

struct gpr_som_elem {
    float * vect;
};
typedef struct gpr_som_elem gpr_som_element;


struct gpr_som_struct {
    int dimension;
    int no_of_sensors;
    struct gpr_som_elem ** weight;
    float * state;
    float * sensor_scale;
};
typedef struct gpr_som_struct gpr_som;

struct gpr_som_stack_struct {
    int no_of_layers;
    unsigned char ** img;
    int * img_width;
    int * img_height;
    int * patch_width;
    gpr_som * som;
    int inhibit_radius;
    int excite_radius;
    float learning_rate;
    unsigned int random_seed;
    unsigned char initialised;
};
typedef struct gpr_som_stack_struct gpr_som_stack;

void gpr_som_init(int dimension, int no_of_sensors, gpr_som * som);
void gpr_som_init_image_filters(int filters_dimension,
                                int patch_width,
                                gpr_som * som,
                                float max_pixel_value,
                                int patch_dimensions,
                                unsigned int * random_seed);
void gpr_som_free(gpr_som * som);
float * gpr_som_get_filter(gpr_som * som, int index);
int gpr_som_init_sensor(gpr_som * som,
                        int sensor_index,
                        float min_value, float max_value,
                        unsigned int * random_seed);
void gpr_som_init_sensors(gpr_som * som,
                          float min_value, float max_value,
                          unsigned int * random_seed);
int gpr_som_init_sensor_from_data(gpr_som * som,
                                  int sensor_index,
                                  int training_data_field_index,
                                  float * training_data,
                                  int training_data_fields_per_example,
                                  int no_of_training_examples,
                                  unsigned int * random_seed);
void gpr_som_init_filters_from_image(gpr_som * som,
                                     unsigned char * img,
                                     int img_width, int img_height,
                                     int bitsperpixel,
                                     int patch_dimensions,
                                     unsigned int * random_seed);
void gpr_som_learn_from_data(gpr_som * som,
                             int * data_field_index,
                             float * training_data,
                             int fields_per_sample,
                             int no_of_samples,
                             int learning_itterations,
                             int inhibit_radius, int excite_radius,
                             float learning_rate,
                             unsigned int * random_seed,
                             int show_progress);
void gpr_som_outputs_from_data(gpr_som * som,
                               int * data_field_index,
                               float * data,
                               int fields_per_sample,
                               int no_of_samples,
                               float * result);
void gpr_som_learn(gpr_som * som,
                   float * sensors,
                   int inhibit_radius,
                   int excite_radius,
                   float learning_rate);
void gpr_som_learn_from_image(gpr_som * som,
                              unsigned char * img,
                              int img_width, int img_height,
                              int bitsperpixel,
                              int patch_dimensions,
                              int tx, int ty,
                              int inhibit_radius,
                              int excite_radius,
                              float learning_rate);
void gpr_som_learn_filters_from_image(int no_of_samples,
                                      unsigned char * img,
                                      int img_width, int img_height,
                                      int bitsperpixel,
                                      int patch_dimensions,
                                      gpr_som * som,
                                      unsigned int * random_seed,
                                      int inhibit_radius,
                                      int excite_radius,
                                      float learning_rate);
int gpr_som_update(float * sensors,
                   gpr_som * som,
                   float * x, float * y);
int gpr_som_update_from_image(unsigned char * img,
                              int img_width, int img_height,
                              int bitsperpixel,
                              int tx, int ty,
                              gpr_som * som,
                              int patch_dimensions,
                              float * x, float * y);
int gpr_som_run(float * sensors,
                gpr_som * som,
                float * x, float * y);
int gpr_som_run_from_image(unsigned char * img,
                           int img_width, int img_height,
                           int bitsperpixel,
                           int tx, int ty,
                           gpr_som * som,
                           int patch_dimensions,
                           float * x, float * y);
void gpr_som_save(gpr_som * som,
                  FILE * fp);
void gpr_som_load(gpr_som * som,
                  FILE * fp);
void gpr_som_show_state(gpr_som * som,
                        unsigned char * img,
                        int img_width, int img_height,
                        int bitsperpixel);
void gpr_som_show_filters(gpr_som * som,
                          unsigned char * img,
                          int img_width, int img_height,
                          int bitsperpixel,
                          int patch_dimensions);

void gpr_som_stack_init(int no_of_layers,
                        int img_width, int img_height,
                        int bitsperpixel,
                        int patch_width,
                        int * som_dimension,
                        gpr_som_stack * stack,
                        unsigned int random_seed);

void gpr_som_stack_free(gpr_som_stack * stack);

void gpr_som_stack_learn(int no_of_samples,
                         unsigned char * img,
                         int img_width, int img_height,
                         int bitsperpixel,
                         gpr_som_stack * stack);

/* integralimage.c */

void update_integral_image(unsigned char * img,
                           int img_width, int img_height,
                           int bitsperpixel,
                           long * integral_image);

long get_integral(long * integral_image,
                  int tx, int ty, int bx, int by, int img_width);

/* centresurround.c */

void update_centre_surround(unsigned char * img,
                            int img_width, int img_height,
                            int bitsperpixel,
                            long * integral_image,
                            int patch_width,
                            unsigned char * output_img,
                            int output_img_width,
                            int output_img_height);
/* resize.h */
int enlarge_image(unsigned char img[], int width, int height,
                  int bitsperpixel,
                  unsigned char enlarged[],
                  int enlarged_width, int enlarged_height);
int resize_image(unsigned char img[], int width, int height,
                 int bitsperpixel,
                 unsigned char resized[],
                 int resized_width, int resized_height);

/* centre_of_gravity.c */
void centre_of_gravity(unsigned char * img,
                       int img_width, int img_height,
                       int bitsperpixel,
                       int * cx, int * cy);
/* rotation.c */
void create_rotation_lookup(int img_width, int img_height,
                            int no_of_rotations,
                            short lookup[]);
int match_image_with_rotation(int img_width, int img_height,
                              unsigned char input[],
                              unsigned char template[], short rotation_lookup[],
                              int no_of_rotations,
                              int *offset_x, int *offset_y, float * score);
void learn_image_with_rotation(int img_width, int img_height,
                               unsigned char input[],
                               unsigned char template[], short rotation_lookup[],
                               int no_of_rotations,
                               int offset_x, int offset_y, int rotation_index);

/* draw.c */
void draw_line(unsigned char img[],
               unsigned int width, unsigned int height,
               int bitsperpixel,
               int tx, int ty, int bx, int by,
               int line_width,
               int r, int g, int b);
void draw_curve(unsigned char img[],
                unsigned int width, unsigned int height,
                int bitsperpixel,
                int x1, int y1,
                int x2, int y2,
                int x3, int y3,
                int line_width,
                int r, int g, int b);

/* symmetry.c */
int detect_vertical_symmetry(unsigned char img[],
                             unsigned int width, unsigned int height,
                             int bitsperpixel,
                             int * tx, int * ty, int * bx, int * by,
                             int include_intensity);

#endif
