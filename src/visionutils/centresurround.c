/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Visionutils - centre/surround functions
 *  Copyright (c) 2013-2015, Bob Mottram
 *  bob@freedombone.net
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "visionutils.h"

/* this is similar to a difference of gaussian (DoG) filter */
void update_centre_surround(unsigned char * img,
                            int img_width, int img_height,
                            int bitsperpixel,
                            long * integral_image,
                            int patch_width,
                            unsigned char * output_img,
                            int output_img_width,
                            int output_img_height)
{
    int x, y, x0, y0, n=0, tx, ty, bx, by, c;
    int outer = patch_width/2;
    int inner = patch_width/4;
    int patch_pixels = patch_width*patch_width/2;
    long response_outer, response_inner, v;
    int bytesperpixel = bitsperpixel/8;

    /* update the integral image */
    update_integral_image(img, img_width, img_height, bitsperpixel,
                          integral_image);

    for (y = 0; y < output_img_height; y++) {
        y0 = y * img_height / output_img_height;
        ty = y0 - outer;
        by = ty + patch_width;
        if (ty < 0) ty = 0;
        if (by >= img_height) by = img_height-1;

        for (x = 0; x < output_img_width; x++) {
            n = y*output_img_width + x;

            x0 = x * img_width / output_img_width;
            tx = x0 - outer;
            bx = tx + patch_width;
            if (tx < 0) tx = 0;
            if (bx >= img_width) bx = img_width-1;

            response_outer =
                get_integral(integral_image,
                             tx, ty, bx, by, img_width);
            response_inner =
                get_integral(integral_image,
                             tx + inner, ty + inner,
                             tx + inner + outer, ty + inner + outer,
                             img_width)*4;
            v = 127 + ((response_inner - response_outer)/
                       patch_pixels);
            if (v < 0) v = 0;
            if (v > 255) v = 255;
            for (c = 0; c < bytesperpixel; c++) {
                output_img[n*bytesperpixel + c] = (unsigned char)v;
            }
        }
    }
}
