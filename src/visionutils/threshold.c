/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Visionutils - example computer vision functions
 *  Copyright (c) 2011-2015, Bob Mottram
 *  bob@freedombone.net
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/


#include "visionutils.h"

/* apply an adaptive threshold to produce a binary image */
void adaptive_threshold(unsigned char * img, int width, int height,
                        int bitsperpixel, int radius,
                        unsigned char * thresholded)
{
    int histogram[256];
    int i,x,y,n,reflectance,threshold;
    int bytesperpixel = bitsperpixel/8;

    memset((void*)thresholded,'\0',
           width*height*sizeof(unsigned char));

    for (y = 0; y < height; y++) {
        n = y*width;
        for (x = 0; x < width; x++,n++) {

            region_histogram(img, width, height,
                             x-radius, y-radius, x+radius, y+radius,
                             bitsperpixel, histogram);

            threshold = histogram_mean_reflectance(histogram);

            reflectance=0;
            for (i = 0; i < bytesperpixel; i++) {
                reflectance += img[n*bytesperpixel+i];
            }
            reflectance /= bytesperpixel;

            if (reflectance > threshold) {
                thresholded[n] = 255;
            }
        }
    }
}

int meanlight_threshold(unsigned char * img, int width, int height,
                        int bitsperpixel, int threshold,
                        unsigned char * thresholded)
{
    unsigned char * img_mono = thresholded;
    unsigned char dark=0,light=0;
    unsigned int i, percentwhite=0;
    unsigned char thresh;

    colour_to_mono(img, width, height, bitsperpixel, img_mono);

    darklight(img_mono, width, height, 1, 50, &dark, &light);

    if ((int)light + threshold > 255)
        thresh = (unsigned char)255;
    else
        thresh = (unsigned char)((int)light - threshold);

    for (i = 0; i < (unsigned int)(width*height); i++)
        if (img_mono[i] >= thresh)
            thresholded[i] = 0;
        else {
            thresholded[i] = 255;
            percentwhite++;
        }

    mono_to_colour(img_mono, width, height, bitsperpixel, img);
    return percentwhite*100/(width*height);
}
