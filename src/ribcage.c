/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Functions for detecting the ribcage within x-ray images
 *  Copyright (c) 2018, Bob Mottram
 *  bob@freedombone.net
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "skeleton.h"

/* maximum horizontal extent of a segment as a percentage of the image width */
#define RIBCAGE_MAX_HORIZONTAL_PERCENT 5

/**
 * @brief Creates four coordinates for the left and right outer ribs.
 *        This is easier to deal with than the many points within the
 *        ribcage array.
 * @param s Skeleton object
 * @return zero on success
 */
static int locate_ribs_outer(skeleton * s)
{
    int i;

    for (i = 0; i < RIBCAGE_SAMPLES; i++)
        if (s->ribcage[i*5] == 0)
            break;

    if (i == 0)
        return 1;

    /* ribs perimeter on the left */

    s->ribs_outer_left[(0*2)+0] = s->ribcage[0*5];
    s->ribs_outer_left[(0*2)+1] = s->ribcage[(0*5)+3];

    s->ribs_outer_left[(1*2)+0] = s->ribcage[(i/4)*5];
    s->ribs_outer_left[(1*2)+1] = s->ribcage[(i/4)*5+3];

    s->ribs_outer_left[(2*2)+0] = s->ribcage[(i/2)*5];
    s->ribs_outer_left[(2*2)+1] = s->ribcage[(i/2)*5+3];

    s->ribs_outer_left[(3*2)+0] = s->ribcage[(i-1)*5];
    s->ribs_outer_left[(3*2)+1] = s->ribcage[(i-1)*5+3];

    /* ribs perimeter on the right */

    s->ribs_outer_right[(0*2)+0] = s->ribcage[(0*5)+2];
    s->ribs_outer_right[(0*2)+1] = s->ribcage[(0*5)+3];

    s->ribs_outer_right[(1*2)+0] = s->ribcage[(i/4)*5+2];
    s->ribs_outer_right[(1*2)+1] = s->ribcage[(i/4)*5+3];

    s->ribs_outer_right[(2*2)+0] = s->ribcage[(i/2)*5+2];
    s->ribs_outer_right[(2*2)+1] = s->ribcage[(i/2)*5+3];

    s->ribs_outer_right[(3*2)+0] = s->ribcage[(i-1)*5+2];
    s->ribs_outer_right[(3*2)+1] = s->ribcage[(i-1)*5+3];

    return 0;
}

/**
 * @brief Post-processing of the rib detections knocks out anything
 *        which is oversized or oriented in the wrong direction.
 *        It then reconstructs the remaining ribcage points in sequence.
 * @param img Array containing x-ray image
 * @param width Width of the image
 * @param height Height of the image
 * @param bitsperpixel Number of bits per pixel
 * @param s Structure storing detected skeleton parameters
 * @return zero on success
 */
static int ribcage_postprocess(unsigned char img[],
                               unsigned int width, unsigned int height,
                               int bitsperpixel,
                               int ribcage[])
{
    int i, i2, j = 0, k, bx, prev_left_x = 0, ctr = 0;
    int ribcage_new[RIBCAGE_SAMPLES*5];

    for (i = 1; i < RIBCAGE_SAMPLES; i++) {
        if ((ribcage[(i-1)*5+4] > 0) &&
            (ribcage[i*5+4] > 0)) {
            bx = ribcage[i*5];

            prev_left_x = bx;

            bx = ribcage[i*5+2];

            /* add an entry to the new array */
            for (k = 0; k < 5; k++) {
                ribcage_new[j*5+k] = ribcage[i*5+k];
            }
            j++;

            ctr = 0;
        }
        else {
            if ((prev_left_x > 0) &&
                (ribcage[i*5+4] > 0) &&
                (ctr < 3)) {
                bx = ribcage[i*5];

                if (abs(bx - prev_left_x) <
                    (int)width*RIBCAGE_MAX_HORIZONTAL_PERCENT/100) {
                    /* add an entry to the new array */
                    for (k = 0; k < 5; k++) {
                        ribcage_new[j*5+k] = ribcage[i*5+k];
                    }
                    j++;
                    prev_left_x = bx;
                }
            }
            ctr++;
        }
    }
    ribcage_new[j*5] = 0;

    /* update the ribcage array with the results */
    for (i = 0; i <= j; i++) {
        for (k = 0; k < 5; k++) {
            ribcage[i*5+k] = ribcage_new[i*5+k];
        }
    }

    /* remove anything oriented in the wrong direction
       at the end of the sequence */
    ctr = 0;
    for (i = j; i >= 1; i--, ctr++) {
        if (ribcage[i*5] > ribcage[(i-1)*5]) {
            ribcage[i*5] = 0;
            j = i;
            break;
        }
        if (ctr > 2)
            break;
    }
    ctr = 0;
    for (i = 0; i < j ; i++, ctr++) {
        if (ribcage[i*5] > ribcage[(i-1)*5]) {
            for (i2 = 0; i2 <= j-i; i2++) {
                for (k = 0; k < 5; k++) {
                    ribcage[i2*5+k] = ribcage[(i2+i)*5+k];
                }
            }
            break;
        }
        if (ctr > 2)
            break;
    }

    return 0;
}

/**
 * @brief deletcts the left and right sides of the ribcage
 * @param img Array containing x-ray image
 * @param width Width of the image
 * @param height Height of the image
 * @param bitsperpixel Number of bits per pixel
 * @param s Structure storing detected skeleton parameters
 * @return zero on success
 */
int detect_ribcage(unsigned char img[],
                   unsigned int width, unsigned int height,
                   int bitsperpixel,
                   skeleton * s)
{
    int i, j, x, y, search_x, intensity, max_intensity;
    int n1, n2;
    int spine_dx = s->spine_bx - s->spine_tx;
    int spine_dy = s->spine_by - s->spine_ty;
    int search_width, av_intensity = 0;
    int bytes_per_pixel = bitsperpixel/8;

    /* store the detection image resolution so that scaling to the
       full resolution image is later possible */
    s->ribcage_detection_image_width = width;
    s->ribcage_detection_image_height = height;

    /* this allows for clearance of the spine
       If it's detecting the spine then increase this value */
    int spine_clearance = (int)width/10;

    for (i = 0; i < RIBCAGE_SAMPLES; i++) {
        /* y coord on the spinal axis */
        y = ((int)height*1/10) + (((int)height*8/10)*i/RIBCAGE_SAMPLES);

        /* x coord on the spinal axis */
        x = s->spine_tx + (y * spine_dx / spine_dy);

        /* how far to search left and right */
        search_width = x;
        if (width-x < search_width)
            search_width = width-x;

        /* adjust for the dead region around the spine, otherwise
           it will be detected as a high intensity region */
        search_width -= spine_clearance;

        max_intensity = 0;

        s->ribcage[i*5] = 0;
        s->ribcage[i*5+1] = 0;
        s->ribcage[i*5+2] = 0;
        s->ribcage[i*5+3] = 0;

        for (search_x = 0; search_x < search_width; search_x++) {
            /* right side array index */
            n1 = y*width + x + spine_clearance + search_x;
            /* left side array index */
            n2 = y*width + x - spine_clearance - search_x;

            /* Calculate the total intensity for a few pixels on the
               left and right */
            intensity = 0;
            for (j = 0; j < 4; j++) {
                intensity +=
                    img[(n1 - j)*bytes_per_pixel] +
                    img[(n2 + j)*bytes_per_pixel];
            }

            /* ...which are preceded by low intensity ones,
               so that we don't get hung up on big high
               intensity areas of the image */
            for (j = 4; j < 7; j++) {
                intensity +=
                    (255 - img[(n1 - j)*bytes_per_pixel]) +
                    (255 - img[(n2 + j)*bytes_per_pixel]);
            }

            /* is this the best result on the current scanline? */
            if (intensity > max_intensity) {
                max_intensity = intensity;
                s->ribcage[i*5] = x - spine_clearance - search_x;
                s->ribcage[i*5+1] = x;
                s->ribcage[i*5+2] = x + spine_clearance + search_x;
                s->ribcage[i*5+3] = y;
                s->ribcage[i*5+4] = intensity;
            }
        }
    }

    /* Now we knock out anything which is obviously not valid */

    /* calculate the average detected intensity */
    for (i = 0; i < RIBCAGE_SAMPLES; i++) {
        av_intensity += s->ribcage[i*5+4];
    }
    av_intensity /= RIBCAGE_SAMPLES;

    /* Remove any segments which are of below average intensity */
    for (i = 0; i < RIBCAGE_SAMPLES; i++) {
        if (s->ribcage[i*5+4] < av_intensity*90/100)
            s->ribcage[i*5+4] = 0;
    }

    for (i = 1; i < RIBCAGE_SAMPLES; i++) {
        /* remove anything close to the left or right border */
        if ((s->ribcage[i*5] < 10) ||
            (s->ribcage[i*5+2] >= width-10)) {
            s->ribcage[(i-1)*5+4] = 0;
            s->ribcage[i*5+4] = 0;
            continue;
        }

        /* remove anything oriented in the wrong direction */
        if (s->ribcage[i*5] > s->ribcage[(i-1)*5]) {
            s->ribcage[(i-1)*5+4] = 0;
            s->ribcage[i*5+4] = 0;
            continue;
        }

        /* remove segments wider than we expect */
        if (abs(s->ribcage[i*5] - s->ribcage[(i-1)*5]) >
            (int)width*RIBCAGE_MAX_HORIZONTAL_PERCENT/100) {
            s->ribcage[(i-1)*5+4] = 0;
            s->ribcage[i*5+4] = 0;
            continue;
        }
    }

    /* do some additional processing to tidy up the knockouts
       which happened earlier */
    ribcage_postprocess(img,
                        width, height,
                        bitsperpixel, s->ribcage);

    /* convert the detected segments into simple three line sections
       for left and right rib perimeters */
    locate_ribs_outer(s);

    return 0;
}

/**
 * @brief Draws the outer ribcage detected countours
 * @param img Array containing x-ray image
 * @param width Width of the image
 * @param height Height of the image
 * @param bitsperpixel Number of bits per pixel
 * @param s Structure storing detected skeleton parameters
 * @return zero on success
 */
int draw_ribcage_segments(unsigned char img[],
                          unsigned int width, unsigned int height,
                          int bitsperpixel,
                          skeleton * s)
{
    int i, tx, ty, bx, by;

    if (s->ribcage[0] == 0)
        return 0;

    for (i = 1; i < RIBCAGE_SAMPLES; i++) {
        if (s->ribcage[i*5] == 0)
            break;

        tx = s->ribcage[(i-1)*5];
        ty = s->ribcage[(i-1)*5+3];
        bx = s->ribcage[i*5];
        by = s->ribcage[i*5+3];

        draw_line(img, width, height, bitsperpixel,
                  tx, ty, bx, by, 5, 255, 255, 0);

        tx = s->ribcage[(i-1)*5+2];
        ty = s->ribcage[(i-1)*5+3];
        bx = s->ribcage[i*5+2];
        by = s->ribcage[i*5+3];

        draw_line(img, width, height, bitsperpixel,
                  tx, ty, bx, by, 5, 255, 255, 0);
    }

    return 0;
}

/**
 * @brief Draws the outer ribcage countours
 * @param img Array containing x-ray image
 * @param width Width of the image
 * @param height Height of the image
 * @param bitsperpixel Number of bits per pixel
 * @param s Structure storing detected skeleton parameters
 * @return zero on success
 */
int draw_ribcage(unsigned char img[],
                 unsigned int width, unsigned int height,
                 int bitsperpixel,
                 skeleton * s)
{
    int i, x1, y1, x2, y2;

    for (i = 1; i < 4; i++) {
        x1 = s->ribs_outer_left[(2*(i-1))+0];
        y1 = s->ribs_outer_left[(2*(i-1))+1];
        x2 = s->ribs_outer_left[(2*i)+0];
        y2 = s->ribs_outer_left[(2*i)+1];
        draw_line(img, width, height, bitsperpixel,
                  x1, y1, x2, y2,
                  4, 0, 255, 255);

        x1 = s->ribs_outer_right[(2*(i-1))+0];
        y1 = s->ribs_outer_right[(2*(i-1))+1];
        x2 = s->ribs_outer_right[(2*i)+0];
        y2 = s->ribs_outer_right[(2*i)+1];
        draw_line(img, width, height, bitsperpixel,
                  x1, y1, x2, y2,
                  4, 0, 255, 255);
    }

    return 0;
}

/**
 * @brief Creates a mono image of the ribcage
 * @param full_resolution_img The original full resolution image
 * @param full_resolution_width Width of the full resolution image
 * @param full_resolution_height Height of the full resolution image
 * @param pitsperpixel Number of bits per pixel
 * @param output_img The mono image to be created
 * @param output_width Width of the image to be created
 * @param output_height Height of the image to be created
 * @param s Object containing skeleton parameters
 * @return zero on success
 */
int ribcage_extract(unsigned char full_resolution_img[],
                    unsigned int full_resolution_width, unsigned int full_resolution_height,
                    int bitsperpixel,
                    unsigned char output_img[],
                    unsigned int output_width, unsigned int output_height,
                    skeleton * s)
{
    int i, x1_left, y1_left, x2_left, y2_left;
    int x1_right, x2_right, x_offset;
    int x_full, y_full, x_output, y_output;
    int y_output_start, y_output_end;
    int x_full_left, x_full_right;
    int n_output, n_full, output_width_scaled;
    int bytes_per_pixel = bitsperpixel/8;

    /* clear the output image */
    memset(output_img, '\0', output_width*output_height);

    for (i = 1; i < 4; i++) {
        /* get coordinates scaled into the full sized image */
        x1_left = (int)((unsigned int)s->ribs_outer_left[(2*(i-1))+0] * full_resolution_width / s->ribcage_detection_image_width);
        y1_left = (int)((unsigned int)s->ribs_outer_left[(2*(i-1))+1] * full_resolution_height / s->ribcage_detection_image_height);
        x2_left = (int)((unsigned int)s->ribs_outer_left[(2*i)+0] * full_resolution_width / s->ribcage_detection_image_width);
        y2_left = (int)((unsigned int)s->ribs_outer_left[(2*i)+1] * full_resolution_height / s->ribcage_detection_image_height);
        x1_right = (int)((unsigned int)s->ribs_outer_right[(2*(i-1))+0] * full_resolution_width / s->ribcage_detection_image_width);
        x2_right = (int)((unsigned int)s->ribs_outer_right[(2*i)+0] * full_resolution_width / s->ribcage_detection_image_width);

        /* the vertical starting and ending locations in the output image */
        y_output_start =
            (s->ribs_outer_left[(2*(i-1))+1] - s->ribs_outer_left[1]) *
            (int)output_height / (s->ribs_outer_left[(2*3)+1]-s->ribs_outer_left[1]);
        y_output_end =
            (s->ribs_outer_left[(2*i)+1] - s->ribs_outer_left[1]) *
            (int)output_height / (s->ribs_outer_left[(2*3)+1]-s->ribs_outer_left[1]);

        /* for every vertical position in the output image within this range */
        for (y_output = y_output_start; y_output < y_output_end; y_output++) {
            /* equivalent coords in the full resolution image */
            y_full = y1_left + ((y_output - y_output_start) * (y2_left - y1_left) / (y_output_end - y_output_start));
            x_full_left = x1_left + ((y_output - y_output_start) * (x2_left - x1_left) / (y_output_end - y_output_start));
            x_full_right = x1_right + ((y_output - y_output_start) * (x2_right - x1_right) / (y_output_end - y_output_start));

            /* scale to avoid to much distortion at the top of the ribcage */
            output_width_scaled = (int)((output_width*70/100) + (y_output * (output_width*30/100) / output_height));

            /* offset used to compensate for the horizontal scaling */
            x_offset = (output_width - output_width_scaled)/2;

            for (x_output = 0; x_output < output_width_scaled; x_output++) {
                /* x coord in the full resolution image */
                x_full = x_full_left + (x_output * (x_full_right - x_full_left) / output_width_scaled);

                /* get the array indexes for the pixels */
                n_output = (y_output*(int)output_width + x_output + x_offset);
                n_full = (y_full*full_resolution_width + x_full)*bytes_per_pixel;

                /* update the output image */
                output_img[n_output] = full_resolution_img[n_full];
            }
        }
    }
    return 0;
}
