/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Detect ribs within a ribcage image
 *  Copyright (c) 2018, Bob Mottram
 *  bob@freedombone.net
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "skeleton.h"

/**
 * @brief Horizontally traces line segments.
 *        It's best not to do this recursively.
 * @param edges_image Mono image containing detected edges
 * @param width Width of the image
 * @param height Height of the image
 * @param x Starting x coordinate
 * @param y Starting y coordinate
 * @param tx Top x coordinate of the bounding box
 * @param ty Top y coordinate of the bounding box
 * @param bx Bottom x coordinate of the bounding box
 * @param by Bottom y coordinate of the bounding box
 * @param direction Whether to trace right or left
 * @param mark Whether to mark traced pixels
 */
void horizontal_follow(unsigned char edges_image[],
                       unsigned int width, unsigned int height,
                       int x, int y,
                       int * tx, int * ty, int * bx, int * by,
                       int direction, int mark)
{
    int n = y*width + x;

    while (edges_image[n] == 0) {
        if ((x <= 2) || (x >= width-3))
            break;

        if ((y <= 2) || (y >= height-3))
            break;

        if (mark != 0)
            edges_image[n] = 32;

        if (x < *tx)
            *tx = x;
        if (y < *ty)
            *ty = y;

        if (x > *bx)
            *bx = x;
        if (y > *by)
            *by = y;

        n = y*width + x + direction;
        if (edges_image[n] == 0) {
            x += direction;
            continue;
        }

        n = y*width + x + direction*2;
        if (edges_image[n] == 0) {
            x += direction*2;
            continue;
        }

        n = y*width + x + direction*3;
        if (edges_image[n] == 0) {
            x += direction*3;
            continue;
        }

        n = (y+1)*width + x + direction;
        if (edges_image[n] == 0) {
            x += direction;
            y++;
            continue;
        }

        n = (y-1)*width + x + direction;
        if (edges_image[n] == 0) {
            x += direction;
            y--;
            continue;
        }

        n = (y+1)*width + x + direction*2;
        if (edges_image[n] == 0) {
            x += direction*2;
            y++;
            continue;
        }

        n = (y-1)*width + x + direction*2;
        if (edges_image[n] == 0) {
            x += direction*2;
            y--;
            continue;
        }

        n = (y+2)*width + x + direction;
        if (edges_image[n] == 0) {
            x += direction;
            y += 2;
            continue;
        }

        n = (y-2)*width + x + direction;
        if (edges_image[n] == 0) {
            x += direction;
            y -= 2;
            continue;
        }

        n = (y+2)*width + x + direction*2;
        if (edges_image[n] == 0) {
            x += direction*2;
            y += 2;
            continue;
        }

        n = (y-2)*width + x + direction*2;
        if (edges_image[n] == 0) {
            x += direction*2;
            y -= 2;
            continue;
        }
    }
}

/**
 * @brief Detects ribs within a standardized ribcage image
 * @param ribcage_img Array containing extracted mono ribcage image
 * @param width Width of the image
 * @param height Height of the image
 * @param output_img Returned mono image of rib lines
 * @return zero on success
 */
int detect_ribs(unsigned char ribcage_img[],
                unsigned int width, unsigned int height,
                unsigned char output_img[])
{
    int x, y, n, tx, ty, bx, by;
    int min_width = (int)width/20;
    int max_vertical = (int)height/10;

    memcpy(output_img, ribcage_img, width*height);

    detect_edges(output_img, width, height, 0, 40);

    for (y = 1; y < height-1; y++) {
        for (x = 1; x < width-1; x++) {
            n = y*width + x;
            if (output_img[n] == 0) {

                tx=x; ty=y; bx=x; by=y;
                horizontal_follow(output_img, width, height, x, y,
                                  &tx, &ty, &bx, &by, 1, 0);
                if ((bx-tx > min_width) || (by-ty > max_vertical)) {
                    tx=x; ty=y; bx=x; by=y;
                    horizontal_follow(output_img, width, height, x, y,
                                      &tx, &ty, &bx, &by, 1, 1);
                }

                tx=x; ty=y; bx=x; by=y;
                horizontal_follow(output_img, width, height, x, y,
                                  &tx, &ty, &bx, &by, -1, 0);
                if ((bx-tx > min_width) || (by-ty > max_vertical)) {
                    tx=x; ty=y; bx=x; by=y;
                    horizontal_follow(output_img, width, height, x, y,
                                      &tx, &ty, &bx, &by, -1, 1);
                }
            }
        }
    }

    return 0;
}

/**
 * @brief Removes horizontally oriented rib lines from an edges image
 * @param output_img Edges image
 * @param width Width of the image
 * @param height Height of the image
 * @return zero on success
 */
int remove_ribs(unsigned char output_img[],
                unsigned int width, unsigned int height)
{
    int x, y, y_range, n;

    /* remove all edges except for ribs */
    for (y = 1; y < height-1; y++) {
        for (x = 1; x < width-1; x++) {
            if (output_img[y*width + x] == 32) {
                /* include a bit of vertical variation to mop
                   up straggling pixels */
                for (y_range = y-1; y_range <= y+1; y_range++) {
                    n = y_range*width + x;
                    output_img[n] = 255;
                }
            }
        }
    }

    return 0;
}
