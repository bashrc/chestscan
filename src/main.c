/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  chestscan - scan x-ray images for pneumonia
 *  Copyright (c) 2018, Bob Mottram
 *  bob@freedombone.net
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include "png2.h"
#include "visionutils.h"
#include "skeleton.h"

static void show_help()
{
    printf("chestscan - a tool for x-ray images\n\n");
    printf(" -f --filename [file]       Filename of a jpg or png x-ray image\n");
    printf(" -d --detect [file]         Filename for image showing detection results\n");
    printf(" -s --segments              Together with the detect option shows segments found\n");
    printf(" -r --ribs [file]           Filename for detection of ribs\n");
    printf(" -o --output [file]         Filename for the image to be produced\n");
    printf(" -t --threshold [value]     Threshold for the output image\n");
    printf(" -v --verbose               Verbose output\n");
    printf("\nExample:\n\n    chestscan -f chest_xray/train/NORMAL/IM-0275-0001.jpeg\n");
}

int main(int argc, char* argv[])
{
    char scan_filename[256];
    char output_filename[256];
    char ribcage_filename[256];
    char ribs_edges_filename[256];
    char command[512];
    int pos, i;
    FILE *fp;
    unsigned char * image_data = NULL;
    unsigned char * standard_image_data = NULL;
    unsigned char * ribcage_image_data = NULL;
    unsigned char * ribs_edges = NULL;
    const unsigned int standard_width=640;
    const unsigned int standard_height=640;
    const unsigned int ribcage_image_width=800;
    const unsigned int ribcage_image_height=600;
    unsigned int image_width=0;
    unsigned int image_height=0;
    unsigned int image_bitsperpixel=0;
    skeleton s;
    int show_segments = 0;
    int image_converted = 0;
    int verbose = 0;
    unsigned char threshold = 80;

    output_filename[0] = 0;
    ribs_edges_filename[0] = 0;

    sprintf(ribcage_filename, "%s", "result.png");

    scan_filename[0] = 0;
    for (i = 1; i < argc; i++) {
        if ((strcmp(argv[i],"-f")==0) ||
            (strcmp(argv[i],"--filename")==0)) {
            i++;
            sprintf(scan_filename, "%s", argv[i]);
        }
        if ((strcmp(argv[i],"-d")==0) ||
            (strcmp(argv[i],"--detect")==0)) {
            i++;
            sprintf(output_filename, "%s", argv[i]);
        }
        if ((strcmp(argv[i],"-o")==0) ||
            (strcmp(argv[i],"--output")==0)) {
            i++;
            sprintf(ribcage_filename, "%s", argv[i]);
        }
        if ((strcmp(argv[i],"-t")==0) ||
            (strcmp(argv[i],"--threshold")==0)) {
            i++;
            threshold = (unsigned char)atoi(argv[i]);
        }
        if ((strcmp(argv[i],"-r")==0) ||
            (strcmp(argv[i],"--ribs")==0)) {
            i++;
            sprintf(ribs_edges_filename, "%s", argv[i]);
        }
        if ((strcmp(argv[i],"-h")==0) ||
            (strcmp(argv[i],"--help")==0)) {
            show_help();
            return 0;
        }
        if ((strcmp(argv[i],"-s")==0) ||
            (strcmp(argv[i],"--segments")==0)) {
            show_segments = 1;
        }
        if ((strcmp(argv[i],"-v")==0) ||
            (strcmp(argv[i],"--verbose")==0)) {
            verbose = 1;
        }
    }

    if (scan_filename[0] == 0) {
        show_help();
        return 0;
    }

    /* check that the file exists */
    fp = fopen(scan_filename, "r");
    if (!fp) {
        printf("%s does not exist\n", scan_filename);
        return 1;
    }
    fclose(fp);

    /* convert from jpeg to png */
    if ((strstr(scan_filename,"jpeg") != NULL) ||
        (strstr(scan_filename,"jpg") != NULL)) {
        if (verbose != 0) {
            printf("Converting jpeg\n");
        }
        sprintf(command, "mogrify -format png %s", scan_filename);
        system(command);

        /* change the file ending */
        pos = strlen(scan_filename)-3;
        if (strstr(scan_filename,"jpeg") != NULL)
            pos = strlen(scan_filename)-4;
        scan_filename[pos++] = 'p';
        scan_filename[pos++] = 'n';
        scan_filename[pos++] = 'g';
        scan_filename[pos] = 0;
        if (verbose != 0) {
            printf("Converted to %s\n", scan_filename);
        }

        /* check that the converted file exists */
        fp = fopen(scan_filename, "r");
        if (!fp) {
            printf("Converted image %s does not exist\n", scan_filename);
            printf("Do you have imagemagick installed?\n");
            return 1;
        }
        fclose(fp);

        image_converted = 1;
    }

    image_data = read_png_file(scan_filename, &image_width, &image_height, &image_bitsperpixel);
    if (image_data == NULL) {
        printf("Couldn't load image %s\n", scan_filename);
        return 0;
    }
    if ((image_width == 0) || (image_height==0)) {
        printf("Couldn't load image size %dx%d\n", image_width, image_height);
        return 0;
    }
    if (image_bitsperpixel == 0) {
        printf("Couldn't load image depth\n");
        return 0;
    }

    if (verbose != 0) {
        printf("Image: %s\n", scan_filename);
        printf("Resolution: %dx%d\n", image_width, image_height);
        printf("Depth: %d\n", image_bitsperpixel);
        printf("Allocating memory for downsampled image\n");
    }
    standard_image_data =
        (unsigned char*)malloc(standard_width*standard_height*
                               (image_bitsperpixel/8));
    if (!standard_image_data) {
        free(image_data);
        return 6;
    }

    if (verbose != 0) {
        printf("Downsampling the original image\n");
    }
    if (resize_image(image_data, image_width, image_height,
                     image_bitsperpixel,
                     standard_image_data,
                     standard_width, standard_height) != 0) {
        printf("Failed to change image resolution\n");
        free(image_data);
        return 2;
    }

    if (verbose != 0) {
        printf("Detecting the spinal axis\n");
    }
    detect_spine(standard_image_data, standard_width, standard_height,
                 image_bitsperpixel, &s);

    if (verbose != 0) {
        printf("Detecting the ribcage\n");
    }
    detect_ribcage(standard_image_data, standard_width, standard_height,
                   image_bitsperpixel, &s);

    if (strlen(output_filename) > 0) {
        /* draw the spinal axis */
        if (verbose != 0) {
            printf("Drawing the spinal axis\n");
        }
        draw_line(standard_image_data,
                  standard_width, standard_height, image_bitsperpixel,
                  s.spine_tx, s.spine_ty, s.spine_bx, s.spine_by,
                  3, 0, 255, 0);

        if (show_segments != 0) {
            /* show the segments found when detecting the outer perimeter
               of the rigcage */
            if (verbose != 0) {
                printf("Drawing rigcage detected segments\n");
            }
            draw_ribcage_segments(standard_image_data,
                                  standard_width, standard_height,
                                  image_bitsperpixel, &s);
        }
        else {
            /* draws some lines indicating the perimeter of the ribcage */
            if (verbose != 0) {
                printf("Drawing rigcage perimeter as lines\n");
            }
            draw_ribcage(standard_image_data,
                         standard_width, standard_height,
                         image_bitsperpixel, &s);
        }

        if (verbose != 0) {
            printf("Saving rigcage detection image\n");
        }
        write_png_file(output_filename, standard_width, standard_height, 24,
                       standard_image_data);
    }

    /* create an image to store the extracted ribcage */
    if (verbose != 0) {
        printf("Allocating memory for ribcage image\n");
    }
    ribcage_image_data =
        (unsigned char*)malloc(ribcage_image_width*ribcage_image_height);
    if (!ribcage_image_data) {
        free(image_data);
        free(standard_image_data);
        return 5;
    }

    /* create an image which is only of the ribcage in a standard orientation */
    if (verbose != 0) {
        printf("Extracting ribcage image\n");
    }
    ribcage_extract(image_data, image_width, image_height, image_bitsperpixel,
                    ribcage_image_data, ribcage_image_width, ribcage_image_height,
                    &s);

    /* adjust the contrast of the ribcage image to compensate for
       differing exposires */
    if (verbose != 0) {
        printf("Adjusting contrast\n");
    }
    dark_light_contrast(ribcage_image_data,
                        ribcage_image_width, ribcage_image_height, 8, threshold);

    if (verbose != 0) {
        printf("Saving ribcage image\n");
    }
    write_png_file(ribcage_filename, ribcage_image_width, ribcage_image_height, 8,
                   ribcage_image_data);


    if (strlen(ribs_edges_filename) > 0) {
        /* create an image to store the extracted ribcage */
        if (verbose != 0) {
            printf("Allocating memory for edges image\n");
        }
        ribs_edges =
            (unsigned char*)malloc(ribcage_image_width*ribcage_image_height);
        if (!ribs_edges) {
            free(ribcage_image_data);
            free(image_data);
            free(standard_image_data);
            return 5;
        }
        if (verbose != 0) {
            printf("Detecting ribs\n");
        }
        detect_ribs(ribcage_image_data, ribcage_image_width, ribcage_image_height,
                    ribs_edges);

        if (verbose != 0) {
            printf("Saving edges image\n");
        }
        write_png_file(ribs_edges_filename,
                       ribcage_image_width, ribcage_image_height, 8,
                       ribs_edges);
    }

    /* if the image was converted from jpeg then delete the temporary
       png file so that storage space isn't wasted */
    if (image_converted == 1) {
        if (verbose != 0) {
            printf("Removing temporary png file\n");
        }

        sprintf(command, "rm %s", scan_filename);
        system(command);
    }

    /* free image arrays */
    if (ribs_edges != NULL)
        free(ribs_edges);

    free(image_data);
    free(ribcage_image_data);
    free(standard_image_data);

    if (verbose != 0)
        printf("Ended Successfully\n");

    return 0;
}
