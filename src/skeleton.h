/*********************************************************************
* Software License Agreement (BSD License)
*
*  Image processing for skeletons
*  Copyright (c) 2018, Bob Mottram
*  bob@freedombone.net
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/


#ifndef SKELETON_HEADERS_H
#define SKELETON_HEADERS_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <ctype.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include <omp.h>
#include "visionutils.h"

#define RIBCAGE_SAMPLES 80

#define HEATMAP_WIDTH 64
#define HEATMAP_HEIGHT 32

struct skeleton_struct {
    /* resolution of the image used for ribcage detection */
    unsigned int ribcage_detection_image_width;
    unsigned int ribcage_detection_image_height;

    /* line defining the axis of the spine */
    int spine_tx, spine_ty;
    int spine_bx, spine_by;

    /* left and right ribcage coords along the spine */
    int ribcage[RIBCAGE_SAMPLES*5];

    /* four coordinates for the left and right outer ribs */
    int ribs_outer_left[4*2];
    int ribs_outer_right[4*2];

    unsigned int heatmap[HEATMAP_WIDTH*HEATMAP_HEIGHT];
};
typedef struct skeleton_struct skeleton;

int detect_spine(unsigned char img[],
                 unsigned int width, unsigned int height,
                 int bitsperpixel,
                 skeleton * s);

int detect_ribcage(unsigned char img[],
                   unsigned int width, unsigned int height,
                   int bitsperpixel,
                   skeleton * s);
int draw_ribcage_segments(unsigned char img[],
                          unsigned int width, unsigned int height,
                          int bitsperpixel,
                          skeleton * s);
int ribcage_extract(unsigned char full_resolution_img[],
                    unsigned int full_resolution_width, unsigned int full_resolution_height,
                    int bitsperpixel,
                    unsigned char output_img[],
                    unsigned int output_width, unsigned int output_height,
                    skeleton * s);

int draw_ribcage(unsigned char img[],
                 unsigned int width, unsigned int height,
                 int bitsperpixel,
                 skeleton * s);
int dark_light_contrast(unsigned char img[],
                        unsigned int width, unsigned int height,
                        int bitsperpixel,
                        unsigned char threshold);
int detect_ribs(unsigned char ribcage_img[],
                unsigned int width, unsigned int height,
                unsigned char output_img[]);
int remove_ribs(unsigned char output_img[],
                unsigned int width, unsigned int height);

#endif
